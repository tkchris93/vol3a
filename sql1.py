import sqlite3 as sql
import csv
import pandas as pd

def prob1():
    """
    Create the following SQL tables with the following columns:
        -- MajorInfo: MajorID (int), MajorName (string)
        -- CourseInfo CourseID (int), CourseName (string)
    --------------------------------------------------------------
    Do not return anything.  Just create the designated tables.
    """
    db = sql.connect("sql1")
    cur = db.cursor()
    cur.execute('DROP TABLE IF EXISTS MajorInfo')
    cur.execute('DROP TABLE IF EXISTS CourseInfo')
    cur.execute('CREATE TABLE MajorInfo (MajorID int not null, MajorName text not null);')
    cur.execute('CREATE TABLE CourseInfo (CourseID int not null, CourseName text not null);')
    db.commit()
    db.close()

def prob2():
    """
    Create the following SQL table with the following columns:
        -- ICD: ID_Number (int), Gender (string), Age (int) ICD_Code (string)
    --------------------------------------------------------------
    Do not return anything.  Just create the designated table.
    """
    with open("icd9.csv", "rb") as csvfile:
        rows = [tuple(row) for row in csv.reader(csvfile, delimiter=',')]

    db = sql.connect("sql2")
    db.execute('DROP TABLE IF EXISTS ICD')
    db.execute('CREATE TABLE ICD (ID_Number int not null, Gender text not null, Age int not null, ICD_Code text not null);')
    db.executemany('INSERT INTO ICD VALUES(?, ?, ?, ?);', rows)
    db.commit()
    db.close()

def prob3():
    """
    Create the following SQL tables with the following columns:
        -- StudentInformation: StudentID (int), Name (string), MajorCode (int)
        -- StudentGrades: StudentID (int), ClassID (int), Grade (int)

    Populate these tables, as well as the tables from Problem 1, with
        the necesary information.  Also, use the column names for
        MajorInfo and CourseInfo given in Problem 1, NOT the column
        names given in Problem 3.
    ------------------------------------------------------------------------
    Do not return anything.  Just create the designated tables.
    """
    with open("course_info.csv", "rb") as csvfile:
        course_info = [tuple(row) for row in csv.reader(csvfile, delimiter=',')]
    with open("major_info1.csv", "rb") as csvfile:
        major_info = [tuple(row) for row in csv.reader(csvfile, delimiter=',')]
    with open("student_grades1.csv", "rb") as csvfile:
        student_grades = [tuple(row) for row in csv.reader(csvfile, delimiter=',')]
    with open("student_info1.csv", "rb") as csvfile:
        student_info = [tuple(row) for row in csv.reader(csvfile, delimiter=',')]


    db = sql.connect("sql1")
    db.execute("DROP TABLE IF EXISTS StudentInformation")
    db.execute("DROP TABLE IF EXISTS StudentGrades")
    db.execute("CREATE TABLE StudentInformation (StudentID int not null, Name text not null, MajorCode int not null);")
    db.execute("CREATE TABLE StudentGrades (StudentID int not null, ClassID int not null, Grade int not null);")
    db.executemany('INSERT INTO CourseInfo VALUES(?,?);', course_info)
    db.executemany('INSERT INTO MajorInfo VALUES(?,?);', major_info)
    db.executemany('INSERT INTO StudentGrades VALUES(?,?,?);', student_info)
    db.executemany('INSERT INTO StudentInformation VALUES(?,?,?);', student_info)
    db.commit()
    db.close()

def prob4():
    """
    Find the number of men and women, respectively, between ages 25 and 35
    (inclusive).
    You may assume that your "sql1" and "sql2" databases have already been
    created.
    ------------------------------------------------------------------------
    Returns:
        (n_men, n_women): A tuple containing number of men and number of women
                            (in that order)
    """
    db = sql.connect("sql2")
    cur = db.cursor()
    cur.execute("SELECT * from ICD WHERE Gender='M' AND Age > 24 AND AGE < 36;")
    n_men = len(cur.fetchall())
    cur.execute("SELECT * from ICD WHERE Gender='F' AND Age > 24 AND AGE < 36;")
    n_women = len(cur.fetchall())
    return n_men, n_women

def useful_test_function(db, query):
    """
    Print out the results of a query in a nice format using pandas
    ------------------------------------------------------------------------
    Inputs:
        db: A sqlite3 database connection
        query: A string containing the SQL query you want to execute
    """
    print pd.read_sql_query(query, db)

prob1()
prob2()
prob3()
print prob4()
