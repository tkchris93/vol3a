import sqlite3 as sql
import csv
import pandas as pd

def prob1():
    """
    Specify relationships between columns in given sql tables.
    """
    print "One-to-one relationships:"
    # Put print statements specifying one-to-one relationships between table
    # columns.

    print "**************************"
    print "One-to-many relationships:"
    # Put print statements specifying one-to-many relationships between table
    # columns.

    print "***************************"
    print "Many-to-Many relationships:"
    # Put print statements specifying many-to-many relationships between table
    # columns.

def prob2():
    """
    Write a SQL query that will output how many students belong to each major,
    including students who don't have a major.

    Return: A table indicating how many students belong to each major.
    """
    #Build your tables and/or query here
    db = sql.connect("sql2")
    cur = db.cursor()
    cur.execute("DROP TABLE IF EXISTS students;")
    cur.execute("DROP TABLE IF EXISTS fields;")
    cur.execute("DROP TABLE IF EXISTS grades")
    cur.execute("DROP TABLE IF EXISTS classes")

    cur.execute("CREATE TABLE students (StudentID int not null, Name text not null, MajorCode int, MinorCode int);")
    cur.execute("CREATE TABLE fields (ID int not null, Name text not null);")
    cur.execute("CREATE TABLE grades (StudentID int not null, ClassID int not null, Grade text);")
    cur.execute("CREATE TABLE classes (ClassID int not null, Name text not null);")

    with open("students.csv", "rb") as csvfile:
        students = [tuple(row[0].split(",")) for row in csv.reader(csvfile, delimiter='.')]
    with open("fields.csv", "rb") as csvfile:
        fields = [tuple(row[0].split(",")) for row in csv.reader(csvfile, delimiter='.')]
    with open("grades.csv", "rb") as csvfile:
        grades = [tuple(row[0].split(",")) for row in csv.reader(csvfile, delimiter='.')]
    with open("classes.csv", "rb") as csvfile:
        classes = [tuple(row[0].split(",")) for row in csv.reader(csvfile, delimiter='.')]

    cur.executemany("INSERT INTO students VALUES(?,?,?,?);", students)
    cur.executemany("INSERT INTO fields VALUES(?,?);", fields)
    cur.executemany("INSERT INTO grades VALUES(?,?,?);", grades)
    cur.executemany("INSERT INTO classes VALUES(?,?);", classes)

    query = "SELECT fields.Name, COUNT(StudentID) FROM students LEFT OUTER JOIN fields ON students.MajorCode = fields.ID GROUP BY MajorCode;"
    # This line will make a pretty table with the results of your query.
        ### query is a string containing your sql query
        ### db is a sql database connection
    result = pd.read_sql_query(query, db)

    db.commit()
    db.close()

    return result


def prob3():
    """
    Select students who received two or more non-Null grades in their classes.

    Return: A table of the students' names and the grades each received.
    """
    #Build your tables and/or query here
    db = sql.connect("sql2")
    # This line will make a pretty table with the results of your query.
        ### query is a string containing your sql query
        ### db is a sql database connection

    query = "SELECT students.Name FROM students LEFT OUTER JOIN grades ON students.StudentID = grades.StudentID GROUP BY students.StudentID HAVING COUNT(*)>2;"
    result =  pd.read_sql_query(query, db)

    return result


def prob4():
    """
    Get the average GPA at the school using the given tables.

    Return: A float representing the average GPA, rounded to 2 decimal places.
    """
    db = sql.connect("sql2")
    cur = db.cursor()

    cur.execute("""SELECT ROUND(AVG(CASE grades.Grade
                    WHEN 'A' THEN 4.0 WHEN 'A-' THEN 4.0
                    WHEN 'B+' THEN 3.0 WHEN 'B' THEN 3.0 WHEN 'B-' THEN 3.0
                    WHEN 'C+' THEN 2.0 WHEN 'C' THEN 2.0 WHEN 'C-' THEN 2.0
                    WHEN 'D+' THEN 1.0 WHEN 'D' THEN 1.0
                    ELSE 0.0
                    END), 2) AS Grade
                FROM grades;""")
    result = cur.fetchall()
    db.close()
    return result[0][0]

def prob5():
    """
    Find all students whose last name begins with 'C' and their majors.

    Return: A table containing the names of the students and their majors.
    """
    #Build your tables and/or query here
    db = sql.connect("sql2")

    query = """SELECT students.Name, fields.Name
            FROM students LEFT OUTER JOIN fields ON students.MajorCode = fields.ID
            WHERE students.Name LIKE '% C%';"""

    # This line will make a pretty table with the results of your query.
        ### query is a string containing your sql query
        ### db is a sql database connection
    result =  pd.read_sql_query(query, db)

    return result
